const APIV1 = '/api/v1'
const APIV2 = '/api/v2'

module.exports = {
    name: '管理后台',
    prefix: 'antdAdmin',
    footerText: 'xxx 版权所有 © 2017 由 cary 支持',
    apiPrefix: '/api/v1',
    YQL: ['http://www.zuimeitianqi.com'],
    CORS:[],
    logo:'/logo.png',
    api:{
        userLogin: `${APIV1}/user/login`
    }
}