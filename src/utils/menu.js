module.exports = [
  {
    id: 1,
    icon: 'laptop',
    name: 'Dashboard',
    router: '/dashboard',
  },
  {
    id: 2,
    bpid: 1,
    name: 'User Manage',
    icon: 'user',
    router: '/users',
  }
]