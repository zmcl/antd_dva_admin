import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'dva';
import {Helmet} from 'react-helmet';
import classnames from 'classnames'
import {config, menu} from 'utils';
import {Layout} from '../components';


const { Sider, styles, Header, Footer } = Layout

const App = ({children, location, dispatch, app}) => {
    const {logo} = config
    const {user, darkTheme, siderFold } = app
    console.log(darkTheme)
    if(localStorage.getItem('darkTheme')===null){
        localStorage.setItem('darkTheme', true)
    }
    const headerProps = {

    }
    const siderProps = {
        menu,
        darkTheme,
        siderFold,
        changeTheme(){
            dispatch({type: 'app/switchTheme'})
        },
        changeOpenKeys(openKeys){
            localStorage.setItem('navOpenKeys', JSON.stringify(openKeys))
            dispatch({type: 'app/handleNavOpenKeys', payload:{navOpenKeys: openKeys}})
        }
    }

    return(
        <div>
            <Helmet>
                <title>{config.name}</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="icon" href={logo} type="image/x-icon" />
            </Helmet>
            
            <div className={classnames(styles.layout)}> 
                 <aside className={classnames(styles.sider)}>
                     <Sider {...siderProps} /> 
                 </aside>
                <div className={styles.main}>
                    <Header {...headerProps} />
                    
                    <div className={styles.container}>
                        <div className={styles.content}>
                            {children}
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        </div>
    )
}

App.prototype = {
    children: PropTypes.element.isRequired,
    location: PropTypes.object,
    dispatch: PropTypes.func,
    app: PropTypes.object,
}

export default connect(({app,loading})=>({app,loading}))(App) 


