import React from 'react';
import { Router, Route } from 'dva/router';
import PropType from 'prop-types';
import IndexPage from './routes/IndexPage';
import App from './routes/App';

// 注册model方法
const registerModel = (app, model) => {
  if(!(app._models.filter(m=>m.namespace===model.namespace).lenght === 1)){
    app.model(model)
  }
}

function Routers({history, app}){
  console.log(app);
  const routes = [
    {
      path: '/',
      component: App,
      getIndexRoute(nextState,cb){
        require.ensure([], require=>{
          // registerModel(app, require('./models/dashboard'))
          cb(null, { component: require('./routes/dashboard/')})
        }, 'dashboard')
      },
      childRoutes:[
        {
          path: 'dashboard',
          getComponent(nextState, cb){
            require.ensure([], require=>{
              cb(null, require('./routes/dashboard/'))
            },'dashboard')
          }
        },{
          path: 'login',
          getComponent (nextState, cb) {
            require.ensure([], require => {
              registerModel(app, require('./models/login'))
              cb(null, require('./routes/login/'))
            }, 'login')
          },
        }
      ]
    }
  ]

  return <Router history={history} routes={routes} />
}

// function RouterConfig({ history }) {
//   return (
//     <Router history={history}>
//       <Route path="/" component={IndexPage} />
//     </Router>
//   );
// }

Routers.PropType = {
  history: PropType.object,
  app: PropType.object,
}

export default Routers;
