
export default {

  namespace: 'app',

  state: {
      user:{},
      darkTheme: localStorage.getItem('darkTheme') === 'true',
      siderFold: localStorage.getItem('siderFold') === 'true',
      permissions:{

      },
      menu:[
          {
              id: 1,
              icon: 'laptop',
              name: 'Dashboard',
              router: '/dashboard',
          },
      ]
  },

  subscriptions: {
      // 订阅
    setup({ dispatch, history }) {  // eslint-disable-line
        dispatch({type: 'query'})
        let tid
        if( localStorage.getItem('siderFold') === null){
            localStorage.setItem('siderFold', false)
        }
        window.onreset=()=>{
            clearTimeout(tid)
            tid = setTimeout(()=>{
                dispatch({type: 'changeNavbar'})
            })
        }
    },
  },

  effects: {
      //获取数据，触发action
    *fetch({ payload }, { call, put }) {  // eslint-disable-line
      yield put({ type: 'save' });
    },
  },

  reducers: {
      //修改state
    save(state, action) {
      return { ...state, ...action.payload };
    },
    switchTheme(state){
        localStorage.setItem('darkTheme', !state.darkTheme)
        return{
            ...state,
            darkTheme: !state.darkTheme,
        }
    },
    handleNavOpenKeys(state, {payload: navOpenKeys}){
        return{
            ...state,
            ...navOpenKeys,
        }
    }
  },

};
