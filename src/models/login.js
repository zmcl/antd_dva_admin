import { login as loginService } from '../services/login'
import { routerRedux } from 'dva/router';
import {queryURL} from 'utils';



export default {
  namespace: 'login',
  state: {
    loginLoading: false,
  },

  effects: {
    *login ({
      payload:values,
    }, { put, call }) {
      yield put({ type: 'showLoginLoading' })
      const data = yield call(loginService, values)
      yield put({type: 'hideLoginLoading'})
      if(data.success){
        const form = queryURL('form')
        yield put({type:'app/query'})
        if(form){
          yield put(routerRedux.push(form))
        }else{
          yield put(routerRedux.push('/dashboard'))
        }
      }else{
        throw data
        // console.log(data)
      }
      
    },
  },
  reducers: {
    showLoginLoading (state) {
      return {
        ...state,
        loginLoading: true,
      }
    },
    hideLoginLoading (state) {
      return {
        ...state,
        loginLoading: false,
      }
    },
  },
}