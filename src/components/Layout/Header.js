import React from 'react'
import PropTypes from 'prop-types'
import {Icon,Menu,Popover} from 'antd'
import styles from './Header.less'

const SubMenu = Menu.SubMenu

const Header = ({}) => {
    return(
        <div className={styles.header}>
            <div className={styles.button}>
                <Icon type={'menu-fold'} />
            </div>
            <div className={styles.rightWarpper}>
                <div className={styles.button}><Icon type='mail' /></div>
                <Menu mode="horizontal">
                    <SubMenu style={{float:'right'}} title={<span><Icon type='user' />userName</span>}>
                        <Menu.Item key='logout'>
                            <a>注销</a>
                        </Menu.Item>
                    </SubMenu>
                </Menu>
            </div>
            
        </div>
    )
}

Header.propTypes = {

}

export default Header