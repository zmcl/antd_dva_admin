import React from 'react'
import PropTypes from 'prop-types'
import {Menu, Icon} from 'antd'
import {Link } from 'dva/router'
import pathToRegexp from 'path-to-regexp'
const { SubMenu } = Menu;

const Menus = ({siderFold, darkTheme, location, handleClickNavMenu, navOpenKeys, changeOpenkeys, menu}) => {
    // const menuTree = ''
    console.log(darkTheme)

    return (
        <Menu
            mode={'inline'}
            theme={darkTheme? 'dark': 'light'}
            defaultSelectedKeys={['1']}
            inlineCollapsed={false}
        >
            <Menu.Item key='1'>
                <Icon type="pie-chart" />
                <span>菜单</span>
            </Menu.Item>
            <Menu.Item key='2'>
                <Icon type="inbox" />
                <span>Option 3</span>
            </Menu.Item>
            <SubMenu key="sub1" title={<span><Icon type="mail" /><span>Navigation One</span></span>}>
                <Menu.Item key="5">Option 5</Menu.Item>
                <Menu.Item key="6">Option 6</Menu.Item>
                <Menu.Item key="7">Option 7</Menu.Item>
                <Menu.Item key="8">Option 8</Menu.Item>
            </SubMenu>
        </Menu>
    )
}

export default Menus