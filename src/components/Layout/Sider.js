import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Switch } from 'antd'
import styles from './Layout.less'
import {config} from 'utils'
import Menus from './Menu'

const  Sider= ({menu, darkTheme, siderFold})=>{
    console.log(menu, darkTheme)
    const menusProps ={
        menu,
        darkTheme,
    }
    return (
        <div >
            <div className={styles.logo}>
                <img src={config.logo} alt="logo"/>
                {siderFold ? '' : <span>{config.name}</span>}
            </div>
            <Menus {...menusProps}/>
        </div>
        
    )
}

Sider.prototype ={

}

export default Sider