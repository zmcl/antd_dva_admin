import Sider from './Sider'
import styles from './Layout.less'
import Header from './Header'
import Footer from './Footer'

export { 
    Sider,
    styles,
    Header,
    Footer,
 }